# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_he.po to Hebrew
# Hebrew messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Translations from iso-codes:
# Translations taken from ICU SVN on 2007-09-09
# Translations taken from KDE:
# Amit Dovev <debian.translator@gmail.com>, 2007.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Omer Zak <w1@zak.co.il>, 2008, 2010, 2012, 2013.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Free Software Foundation, Inc., 2002,2004.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
# Meni Livne <livne@kde.org>, 2000.
# Free Software Foundation, Inc., 2002,2003.
# Meni Livne <livne@kde.org>, 2000.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Lior Kaplan <kaplan@debian.org>, 2004-2007, 2008, 2010, 2011, 2015, 2017.
# Yaron Shahrabani <sh.yaron@gmail.com>, 2018, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: mdcfg@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:05+0000\n"
"PO-Revision-Date: 2020-07-26 03:41+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew (https://www.transifex.com/yaron/teams/79473/he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../mdcfg-utils.templates:1001
msgid "Configure MD devices"
msgstr "הגדרת התקנים מרובי כוננים (MD)"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid "Multidisk (MD) not available"
msgstr "התקנים מרובי כוננים (MD) לא זמינים"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid ""
"The current kernel doesn't seem to support multidisk devices. This should be "
"solved by loading the needed modules."
msgstr ""
"הליבה הנוכחית לא מכילה תמיכה בהתקנים מרובי כוננים. ניתן לפתור זאת על ידי "
"טעינת המודולים הנדרשים."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Create MD device"
msgstr "יצירת התקן מרובה כוננים (MD)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Delete MD device"
msgstr "מחיקת התקן מרובה כוננים (MD)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Finish"
msgstr "סיום"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "Multidisk configuration actions"
msgstr "פעולות הגדרת התקנים מרובי כוננים"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "This is the Multidisk (MD) and software RAID configuration menu."
msgstr "זהו תפריט להגדרת התקנים מרובי כוננים (MD) ו־RAID מבוסס תכנה."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid ""
"Please select one of the proposed actions to configure multidisk devices."
msgstr "נא לבחור את אחת הפעולות הבאות להגדרת התקנים מרובי כוננים."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid "No RAID partitions available"
msgstr "אין מחיצות RAID זמינות"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"No unused partitions of the type \"Linux RAID Autodetect\" are available. "
"Please create such a partition, or delete an already used multidisk device "
"to free its partitions."
msgstr ""
"אין מחיצות פנויות מסוג „Linux RAID Autodetect”. עליך ליצור מחיצה כזאת או "
"למחוק התקן מרובה כוננים שנמצא בשימוש כדי לשחרר את המחיצות שלו."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"If you have such partitions, they might contain actual file systems, and are "
"therefore not available for use by this configuration utility."
msgstr ""
"אם יש לך מחיצות כאלה, הן עלולות להכיל מערכות קבצים אמתיות ולכן לא תהיינה "
"זמינות לשימוש על ידי כלי הגדרות זה."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid "Not enough RAID partitions available"
msgstr "אין מספיק מחיצות RAID זמינות"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid ""
"There are not enough RAID partitions available for your selected "
"configuration.  You have ${NUM_PART} RAID partitions available but your "
"configuration requires ${REQUIRED} partitions."
msgstr ""
"אין מספיק מחיצות RAID זמינות לטובת ההגדרות שבחרת. יש לך ${NUM_PART} מחיצות "
"RAID זמינות, אך ההגדרות שלך דורשות ${REQUIRED} מחיצות."

#. Type: select
#. Choices
#. :sl3:
#. Type: select
#. Choices
#. :sl3:
#: ../mdcfg-utils.templates:6001 ../mdcfg-utils.templates:13001
msgid "Cancel"
msgstr "ביטול"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Multidisk device type:"
msgstr "סוג התקן מרובה כוננים:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Please choose the type of the multidisk device to be created."
msgstr "נא לבחור את סוג ההתקן מרובה הכוננים שייווצר."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid "Active devices for the RAID0 multidisk device:"
msgstr "התקנים פעילים בהתקן מרובה כוננים מסוג RAID0:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid ""
"You have chosen to create a RAID0 array. Please choose the active devices in "
"this array."
msgstr "בחרת ליצור מערך RAID0. כעת יש לבחור את ההתקנים הפעילים במערך זה."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid "Number of active devices for the RAID${LEVEL} array:"
msgstr "מספר ההתקנים הפעילים עבור מערך RAID${LEVEL}:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid ""
"The RAID${LEVEL} array will consist of both active and spare partitions. The "
"active partitions are those used, while the spare devices will only be used "
"if one or more of the active devices fail. A minimum of ${MINIMUM} active "
"devices is required."
msgstr ""
"המערך מסוג RAID${LEVEL} יהיה מורכב ממחיצות פעילות ומחיצות עתודה. המחיצות "
"הפעילות הן אלה שבשימוש, בעוד שמחיצות העתודה תהיינה בשימוש רק אם אחת המחיצות "
"הפעילות תיכשלנה. הכמות המזערית של ההתקנים הפעילים הנדרשת היא ${MINIMUM}."

#. Type: string
#. Description
#. :sl3:
#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001 ../mdcfg-utils.templates:12001
msgid "NOTE: this setting cannot be changed later."
msgstr "הערה: לא ניתן לשנות הגדרות אלה אחר כך."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid "Active devices for the RAID${LEVEL} multidisk device:"
msgstr "התקנים פעילים בהתקן מרובה כוננים מסוג RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} active devices."
msgstr "בחרת ליצור את המערך RAID${LEVEL} עם ${COUNT} התקנים פעילים."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"Please choose which partitions are active devices. You must select exactly "
"${COUNT} partitions."
msgstr ""
"נא לבחור אילו מחיצות הן התקנים פעילים. עליך לבחור בדיוק ${COUNT} מחיצות."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:10001
msgid "Number of spare devices for the RAID${LEVEL} array:"
msgstr "מספר התקני עתודה במערך RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid "Spare devices for the RAID${LEVEL} multidisk device:"
msgstr "התקני עתודה עבור התקן מרובה כוננים מסוג RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} spare devices."
msgstr "בחרת ליצור את המערך RAID${LEVEL} עם ${COUNT} התקני עתודה."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"Please choose which partitions will be used as spare devices. You may choose "
"up to ${COUNT} partitions. If you choose less than ${COUNT} devices, the "
"remaining partitions will be added to the array as \"missing\". You will be "
"able to add them later to the array."
msgstr ""
"נא לבחור אילו מהמחיצות ישמשו כעתודה. ניתן לבחור עד ${COUNT} מחיצות. אם בחרת "
"פחות מ־${COUNT}, השאר תצורפנה למערך כנעדרות. ניתן להוסיף אותן אחר כך למערך."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid "Layout of the RAID10 multidisk device:"
msgstr "פריסת התקן מרובה כוננים מסוג RAID10:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The layout must be n, o, or f (arrangement of the copies) followed by a "
"number (number of copies of each chunk). The number must be smaller or equal "
"to the number of active devices."
msgstr ""
"הפריסה חייבת להיות n,‏ o או f (סידור ההעתקים) שאחריו יבוא מספר (מספר העותקים "
"של כל גוש). המספר חייב להיות קטן או שווה למספר ההתקנים הפעילים."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The letter is the arrangement of the copies:\n"
" n - near copies: Multiple copies of one data block are at similar\n"
"     offsets in different devices.\n"
" f - far copies: Multiple copies have very different offsets\n"
" o - offset copies: Rather than the chunks being duplicated within a\n"
"     stripe, whole stripes are duplicated but are rotated by one\n"
"     device so duplicate blocks are on different devices."
msgstr ""
"האות מציינת את סידור ההעתקים:\n"
"‚n’ - העתקים קרובים: מגוון העתקים של מקטע נתונים אחד נמצאים במיקומים\n"
"        יחסיים דומים בהתקנים שונים.\n"
" ‚f’ - העתקים רחוקים: העתקים של מקטע נמצאים במיקומים יחסיים שונים מאוד.\n"
"‚o’ - העתקים במיקומים יחסיים: במקום לשכפל גושים באותו הפס, פסים שלמים\n"
"        משוכפלים אבל מוסטים התקן אחד הצידה, כך שהעתקים של בלוקים נמצאים\n"
"        בהתקנים שונים."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid "Multidisk device to be deleted:"
msgstr "התקן מרובה כוננים למחיקה:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Deleting a multidisk device will stop it and clear the superblock of all its "
"components."
msgstr ""
"מחיקת התקן מרובה כוננים תפסיק אותו ותמחק את מקטע העל (superblock) של כל "
"רכיביו."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Please note this will not immediately allow you to reuse the partitions or "
"devices in a new multidisk device. The array will however be unusable after "
"the deletion."
msgstr ""
"נא לשים לב שפעולה זאת לא תאפשר לך להשתמש מחדש במחיצות או בהתקנים בהתקן מרובה "
"כוננים חדש באופן מיידי. עם זאת, המערך יהיה לא שמיש לאחר המחיקה."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"If you select a device for deletion, you will get some information about it "
"and you will be given the option of aborting this operation."
msgstr ""
"בעת בחירת התקן למחיקה, יופיעו פרטים עליו ותינתן לך האפשרות לבטל את הפעולה."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices available"
msgstr "אין התקנים מרובי כוננים זמינים"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices are available for deletion."
msgstr "אין התקנים מרובי כוננים זמינים למחיקה."

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid "Really delete this multidisk device?"
msgstr "למחוק את ההתקן מרובה הכוננים הזה?"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
"Please confirm whether you really want to delete the following multidisk "
"device:"
msgstr "נא לאשר אם ברצונך למחוק את ההתקן מרובה הכוננים הבא:"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
" Device:            ${DEVICE}\n"
" Type:              ${TYPE}\n"
" Component devices:"
msgstr ""
" התקן:            ${DEVICE}\n"
" סוג:              ${TYPE}\n"
" התקנים מרכיבים:"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "Failed to delete the multidisk device"
msgstr "מחיקת ההתקן מרובה הכוננים נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "There was an error deleting the multidisk device. It may be in use."
msgstr "התרחשה שגיאה במחיקת התקן מרובה כוננים. יכול להיות שהוא בשימוש."
